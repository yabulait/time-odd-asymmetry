# time-odd-asymmetry

software for measuring naive time odd asymmetry in W+jets event.

## Install 
```
cd time-odd-asymmetry  
mkdir build source
cd source 
asetup AnalysisBase,21.2.127,here
cd ../
source full_compile.sh 
```

### On a new terminal   
```
source setup.sh

```

From now on, we rely on AnalysisTop to make `ntuples` from xAOD.  

## AnalysisTop      
Check this twiki:  
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisTop

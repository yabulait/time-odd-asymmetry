#!/usr/bin/env python
# coding: utf-8

# # Cutflow   
# 
# Cutflow:  check number of events passed a filter (or we offen say cut).  
#           We check relative efficiencies and cumulative efficiencies.  
#           Relative efficiency is a fraction of events passed a filter  
#           compared to the events before this filter. Cumulative efficiencies  
#           is a fraction of events left afther this filter compared to Initial    
#           number of eevents.   
#           
# 
# Software: I use TRDataFrame for my ananlysis.    
# The cell one bellow read root files containing entuples.   
# After read TTree in to TRDataFrame, we can apply few various fileters.  
# Some MC processes sliced, for example W+jets are w-boson pT sliced.  
# So I Scaled each slices with and 36/fb luminosity.

# In[1]:


import ROOT
ROOT.gStyle.SetPalette(61)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)

import os,sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from IPython.core.pylabtools import figsize

sys.path.append("/eos/home-y/yabulait/wjets/")
sys.path.append("/eos/home-y/yabulait/wjets/time-odd-asymmetry")
from context import (Luminosity_fb, Luminosity_pb, grid_date, leptons,mc_processes, samples)
import json
from wjetsAsymmetryFrame import *

import pyrootUtil

event_weights = None
with open("../data/{}/sample_weights.txt".format(grid_date),"r") as f:
    event_weights= json.load( f)

def get_rdf(filePath,branches=np.array(["mcEventWeight", "deta_lj", "Xprep","nbjet", "mT", "qT", "met"])):
    wjets = wjetsAsymmetryFrame()
    df_preSelected = wjets.load_raw_data(files=[filePath], branches=branches)
    dsid = filePath.split('/')[-1]
    dsid = dsid.split('.')[0]
    return  df_preSelected, dsid

datasetsRDF={}
for mc_process in mc_processes:
    _lepton ="el_plus"
    input_dir = "/eos/home-y/yabulait/wjets/ntuples/preselected/{0}/{1}/{2}/".format(mc_process, grid_date,_lepton)
    for dataset in [input_dir+f for f in os.listdir(input_dir)]:
        _df, _dsid = get_rdf(dataset)
        datasetsRDF[_dsid]=_df  #.Filter("(nbjet<1)&&mT>50&&qT>30")

def cut_report(dRDF):
    _cutflowreport={}
    for key in dRDF.keys():
        d = dRDF[key]
        cut = d.Filter("mT>50", "mT>50 GeV").Filter("qT>30", "qT>30 GeV").Filter("nbjet<1", "b-veto").Filter("abs(Xprep)>0.6", "|Xperp|>0.6")
        _cutflowreport[key] = cut.Report()
    return _cutflowreport

def reports_groupBy(reps, processes):
    #processes--> {"wjets": [ids]}
    _grouped={}
    for process, ids in processes.items():
        _process_report = {}
        for iCut in range( len( list(reps[ids[0]])) ):
            Nall = 0
            Npass = 0
            eff  =  0.0
            cumulative = 0.0
            Ninitial = 0
            _name = ""
            for _id in ids:
                _weight = Luminosity_pb*event_weights[_id][1]/event_weights[_id][0]
                _reports = list(reps[_id])
                _name = _reports[iCut].GetName()
                Ninitial = Ninitial + reps[_id].begin().GetAll()*_weight
                Nall = Nall + _reports[iCut].GetAll()*_weight
                Npass = Npass + _reports[iCut].GetPass()*_weight
            _process_report[_name] = {"all":Nall, "pass":Npass, "initial":Ninitial, "eff": Npass/Nall,
                                     "cumulative": Npass/Ninitial}
        _grouped[process] = _process_report
    return _grouped

def print_reports(reportDic):
    _process = list(reportDic.keys())
    _cut_names = reportDic[_process[0]].keys()
    
    for _pro in _process:
        _repors = reportDic[_pro]
        print("{:-^80}".format(_pro))
        for _cut_name in _cut_names:
            _cut = _repors[_cut_name]
            print("{0:<10}: pass={1:<15.1f} all={2:<11.1f} -- eff={3:4.1%} cumulative eff={4:04.1%}"
                  .format(_cut_name, _cut["pass"], _cut["all"], _cut["eff"], _cut["cumulative"]))


# # Cut Flow reports
# 
# TRDataFrame report cutflow on each named filter you applied on data.  
# In the cell bellow we get cut flow report on each sample. After gethered   
# each report we group them by MC processes.

# In[2]:


reports = cut_report(datasetsRDF)
process_groups = {"wjets": ["364170",  "364172",  "364174",  "364176",  "364178",  "364180",
                            "364182",  "364171",  "364173",  "364175",  "364177",  "364179",
                            "364181",  "364183"],
                  "ttbar": ["410470"],
                  "WWlpqq": ["363360"],
                  "singleTop": ["410646", "410647"]}
process_reports = reports_groupBy(reports, process_groups)


# # Print cut flow
# 
# Use custom defined `print_reports` fuction to print out   
# cut flow for each MC process.  

# In[3]:


print_reports(process_reports)


# In[4]:


get_ipython().system(" jupyter nbconvert --output-dir='../docs/' --to html cutFlow.ipynb")
get_ipython().system(" jupyter nbconvert --output-dir='../docs/' --to pdf cutFlow.ipynb")
get_ipython().system(' jupyter nbconvert --to python cutFlow.ipynb')


# In[ ]:





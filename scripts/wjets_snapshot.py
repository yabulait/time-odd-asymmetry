#!/usr/bin/env python
# coding: utf-8

# In[1]:



import ROOT
import os,sys
import numpy as np

sys.path.append("../python/")
from wjetsAsymmetryFrame import *

#set Multi-thread enabled.
ROOT.ROOT.EnableImplicitMT(16)

grid_date="20200508_1230" #20200310_1052 20200410_1450
eos_path = "/eos/home-y/yabulait/wjets/ntuples/grid_output/wjets/{0}/".format(grid_date)
datasets = {}
datasets['Wmunu'] = [d for d in os.listdir(eos_path) if 'Wmunu' in d]
datasets['Wenu'] = [d for d in os.listdir(eos_path) if 'Wenu' in d]
datasets['Wtaunu'] = [d for d in os.listdir(eos_path) if 'Wtaunu' in d]

charge='+'
charge_str = "minus" if charge == '-' else "plus"
def snapshot_preselected(files, channel, output_file):
    wjets = wjetsAsymmetryFrame()
    print("pool size = ",  ROOT.ROOT.GetImplicitMTPoolSize())
    _df = wjets.preselected_data(files=files,channel=channel, charge=charge)

    #catch new RDF pointed to snapshot_test.root
    #"/eos/home-y/yabulait/wjets/root_files/Sherpa_221_preselected.root"
    df_preSelected = wjets.snapshot("nominal", output_file)
    del wjets
    del df_preSelected, _df
    


# In[2]:


datasets['Wenu']


# In[3]:


#W->munu, W->enu
lepton_type={'Wmunu':'muon', 'Wenu':'el'}
for decay_type in ['Wenu']:
    print("%s ----------------"%decay_type)
    for dataset in datasets[decay_type]:
        ds_path = eos_path+dataset+'/'
        files = [ds_path+f for f in os.listdir(ds_path)]
        dsID = dataset.split(".mc16a.")[1][:6]
        save_dir="/eos/home-y/yabulait/wjets/ntuples/preselected/wjets/{0}/{1}_{2}/".format(grid_date,lepton_type[decay_type],charge_str)
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)
        snapshot_preselected(files, lepton_type[decay_type], 
                "{0}/{1}.root".format(save_dir, dsID))
        print (dsID)


# In[4]:


#W->taunu
# run same selection for tau:
leptons={'muon', 'el'}
for dataset in datasets['Wtaunu']:
    ds_path = eos_path+dataset+'/'
    files = [ds_path+f for f in os.listdir(ds_path)]
    dsID = dataset.split(".mc16a.")[1][:6]
    for lep in leptons:
        save_dir="/eos/home-y/yabulait/wjets/ntuples/preselected/wjets/{0}/tau_{1}_{2}/".format(grid_date,lep,charge_str)
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)
        snapshot_preselected(files, "%s"%lep, 
                             "{0}/{1}.root".format(save_dir,dsID))
        print (dsID)


# In[5]:


#ttbar ->l+jets
ttbar_path = "/eos/home-y/yabulait/wjets/ntuples/grid_output/ttbar/{0}/".format(grid_date)
datasets['ttbar'] = [d for d in os.listdir(ttbar_path)]

# run same selection for ttbar:
leptons={'muon', 'el'}
for dataset in datasets['ttbar']:
    print(dataset)
    ds_path = ttbar_path+dataset+'/'
    files = [ds_path+f for f in os.listdir(ds_path)]
    dsID = dataset.split(".mc16a.")[1][:6]
    for lep in leptons:
        save_dir="/eos/home-y/yabulait/wjets/ntuples/preselected/ttbar/{0}/{1}_{2}/".format(grid_date,lep,charge_str)
        if not os.path.isdir(save_dir):
                os.makedirs(save_dir)
        snapshot_preselected(files, "%s"%lep, 
                             "{0}/{1}.root".format(save_dir,dsID))
        print (dsID, lep)


# In[6]:


if charge is '+':
    WplvWmqq_path = "/eos/home-y/yabulait/wjets/ntuples/grid_output/WplvWmqq/{0}/".format(grid_date)
    datasets['WplvWmqq'] = [d for d in os.listdir(WplvWmqq_path)]

    # run same selection for ttbar:
    leptons={'muon', 'el'}
    for dataset in datasets['WplvWmqq']:
        ds_path = WplvWmqq_path+dataset+'/'
        files = [ds_path+f for f in os.listdir(ds_path)]
        dsID = dataset.split(".mc16a.")[1][:6]
        for lep in leptons:
            save_dir="/eos/home-y/yabulait/wjets/ntuples/preselected/WplvWmqq/{0}/{1}_plus/".format(grid_date,lep)
            if not os.path.isdir(save_dir):
                os.makedirs(save_dir)
            snapshot_preselected(files, "%s"%lep, 
                             "{0}/{1}.root".format(save_dir, dsID))
            print (dsID)


# In[3]:


#single top wt inclusive
singleTop_path = "/eos/home-y/yabulait/wjets/ntuples/grid_output/singleTop/{0}/".format(grid_date)
datasets['singleTop'] = [d for d in os.listdir(singleTop_path)]

# run same selection for singleTop:
leptons={'muon', 'el'}
for dataset in datasets['singleTop']:
    print(dataset)
    ds_path = singleTop_path+dataset+'/'
    files = [ds_path+f for f in os.listdir(ds_path)]
    dsID = dataset.split(".mc16a.")[1][:6]
    for lep in leptons:
        save_dir="/eos/home-y/yabulait/wjets/ntuples/preselected/singleTop/{0}/{1}_{2}/".format(grid_date,lep,charge_str)
        if not os.path.isdir(save_dir):
                os.makedirs(save_dir)
        snapshot_preselected(files, "%s"%lep, 
                             "{0}/{1}.root".format(save_dir,dsID))
        print (dsID, lep)


# In[7]:


get_ipython().system('jupyter nbconvert --to python wjets_snapshot.ipynb')


# In[ ]:





# In[ ]:





#!/usr/bin/env python
# coding: utf-8

# # Extract cross section for each MC sample   
# 
# We gether total number of events processed on grid for each samples,  
# and its cross section * filter eff * k-factor.   
# We can see (cross section * filter eff * k-factor) as a sample weight,   
# and we will normalize histograms or number of events to current luminosity    
# by scaling them with this weight.    

# In[4]:


import ROOT
import os,sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from IPython.core.pylabtools import figsize
import os, sys
sys.path.append("/eos/home-y/yabulait/wjets/time-odd-asymmetry")
from context import (Luminosity_fb, Luminosity_pb, grid_date, leptons,mc_processes, samples)
import json


# Load cross section file as a panda object.  
# The file is white space separated.  
# 
# This function get meta datas from each file and add them to get sample specifit meta data.  
# Currenly, w+jets, ttbar and ww->lvnu jj are included. 
# We can add more samples with extend the `filter_xs` variable.

# In[5]:


def weights_xs_included(mc_processes):
    xsections = pd.read_csv("../samples/xsections/PMGxsecDB_mc16.txt", sep="\t\t", index_col=False, 
                        names=["dataset", "physics_short", "crossSection", "genFiltEff", "kFactor", "relUncertUP",
                                "relUncertDOWN", "generator_name", "etag"], 
                            usecols=["dataset", "crossSection", "genFiltEff", "kFactor", 
                                    "relUncertUP", "relUncertDOWN"], skiprows=1)

    #filter_xs = (xsections.dataset.values>364155) * (xsections.dataset.values<364198)
    mcdatasets = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/tight_selection/mu/".format(grid_date)
    dsid_for_filter = []
    for ifile in os.listdir(mcdatasets):
        _dids = ifile.strip(".root")
        if _dids.isdigit():
            dsid_for_filter.append(int(_dids))
    #dsid_for_filter = list(range(364155,364199,1)) + [410470, 363360,410646, 410647]
    filter_xs = xsections.dataset.isin(dsid_for_filter)
    xs_wjets = xsections[filter_xs]
    xs_wjets['weight'] = xs_wjets['crossSection'].to_numpy() * xs_wjets['genFiltEff'].to_numpy()* xs_wjets['kFactor'].to_numpy()
   
    # Readout event count from a single file
    def event_count(grid_output_file):
        try:
            _f = ROOT.TFile.Open(grid_output_file)
            _tree = _f.Get("sumWeights")
            _count = 0.0
            for entry in _tree:
                _count += entry.totalEventsWeighted
            _f.Close()
            return _count
        except:
            print("Can not get event count from %s"%grid_output_file)

    #Usally grid output of a dataset contain many files.
    #We have to read each of them and sum all event counts.
    def total_event(file_dir):
        files = [file_dir+'/'+f for f in os.listdir(file_dir) ]
        count_sum = 0
        for _f in files:
            count_sum = count_sum + event_count(_f)
        return count_sum

    #find out each dataset, files and dataset IDs
    datasets=[]
    grid_output_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/grid_output/".format(grid_date)
    for dset in os.listdir(grid_output_dir):
        datasets.append(grid_output_dir+dset )

    event_weights={}
    for dataset in datasets:
        Nevent = total_event(dataset)
        ds_id = dataset.split(".")[2]
        event_weights[ds_id] = [Nevent, xs_wjets.loc[lambda df: df["dataset"].isin([int(ds_id)]),'weight'].values[0]  ] #xs_wjets['weight'][xs_wjets["dataset"] == int(ds_id)].values[0]
    return event_weights


# In[6]:


event_weights = weights_xs_included(mc_processes)


# In[7]:


weight_pd = pd.DataFrame.from_dict(event_weights, orient="index", columns=["totalEvents", "sample_weights"])


# # Check metadata and save them to a json file

# In[8]:


weight_pd.head()


# Fist column is dataset ID. 
# * totalEvent: total number of generated events for this sample   
# * sample_weight: cross section * filter eff * k-factor

# In[9]:


#save to json
output_loc="../data/{}/".format(grid_date)
if not os.path.isdir(output_loc):
            os.makedirs(output_loc)
with open("{}/sample_weights.txt".format(output_loc),"w") as f:
    json.dump(event_weights, f)


# Regenerate `../data/sample_weights.txt` file once after each grid jobs.  
# This is necessary in case fraction of events are not processed on grid. 
# Otherwise just reload this file, and apply sample weights to each sample,  
# when create histograms and scale number of events to current luminosity.

# In[10]:


#save to json 
sample_weights = None
with open("../data/{}/sample_weights.txt".format(grid_date),"r") as f:
    sample_weights= json.load( f)
    
N = sample_weights['364156'][0]
w = sample_weights['364156'][1]

lumi = 10000 # pb unit
total_w = lumi*(w/N)
#hist.Scale(total_w)


# Keys of `sample_weights` are dataset IDs of MC samples.  
# Each value of `sample_weights` is a size=2 list; first element is   
# total number events, second element is cross section * filter eff * k-factor.  
# 

# In[11]:


get_ipython().system(" jupyter nbconvert --output-dir='../docs/' --to html check_cross_sections.ipynb")
get_ipython().system(" jupyter nbconvert --output-dir='../docs/' --to pdf check_cross_sections.ipynb")
get_ipython().system(' jupyter nbconvert --to python check_cross_sections.ipynb')


# In[ ]:





# In[ ]:





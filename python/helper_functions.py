import os,sys
import numpy as np
import ROOT
import pyrootUtil
import json
from wjetsAsymmetryFrame import wjetsAsymmetryFrame
import pyrootUtil.plotUtil.styles as styles

def get_rdf(filePath,
            branches=None):
    wjets = wjetsAsymmetryFrame()
    try:
        df_preSelected = wjets.load_raw_data(tree_name="nominal", files=[filePath], branches=branches)
    except Exception as inst:
        return None, None
    else:
        dsid = filePath.split('/')[-1]
        dsid = dsid.split('.')[0]
        return  df_preSelected, dsid



def fill_histogram(RDFs, hist, var, weightBR=None):
    histoDic = {}
    for dsid, _df in RDFs.items():
        if weightBR:
            histoDic[dsid] = _df.Histo1D(hist, var, weightBR)
        else:
            histoDic[dsid] = _df.Histo1D(hist, var)
    return histoDic

    
def NormalizeHist(histDic, weightsDic,Luminosity_pb):
    for _id in histDic.keys():
        _weight = Luminosity_pb*weightsDic[_id][1]/weightsDic[_id][0]
        histDic[_id].Scale(_weight)
    return histDic

def groupBy(histDic, processes):
    #processes--> {"wjets": [ids]}
    _hDic={}
    keys = histDic.keys()
    for process, ids in processes.items():
        #print ("process, ids = ", process, ids)
        _hist = None
        for _id in keys:
            if _id not in ids: continue
            if _hist is None:
                _hist = histDic[_id].Clone()
            else:
                _hist.Add(histDic[_id].Clone())
        
        _hDic[process] = _hist
    return _hDic

def draw_atlas_label(x=0.57, y=0.85, label="Internal", Luminosity_fb=33):
    AL = ROOT.TLatex() #l.SetTextAlign(12) l.SetTextSize(tsize)
    AL.SetNDC()
    AL.SetTextFont(72)
    AL.SetTextColor(ROOT.kBlack)
    Inetrnal = ROOT.TLatex()
    Inetrnal.SetNDC()
    Inetrnal.SetTextFont(42)
    Inetrnal.SetTextColor(ROOT.kBlack)
    ## print text
    TL = ROOT.TLatex()
    TL.SetNDC(1)
    AL.DrawLatex(x,y,"ATLAS")
    Inetrnal.DrawLatex(x+0.12,y,label)
    styles.energy_lumi(x,y-0.06,13, Luminosity_fb)

def createColorTable(size=1, color_map=61):
    ROOT.gStyle.SetPalette(color_map)
    _step = int(color_map/size)
    return [ROOT.TColor.GetColorPalette(i*_step) for i in range(0,size)]

sample_colors = createColorTable(size=3, color_map=61)
cwjets, cttbar, cWWlpqq = 0,1,2

def plot_samples(fig, hVarDic, log=None, x_label=None, y_label=None):
    if x_label:
        hVarDic["Wmunu"].GetXaxis().SetTitle(x_label)
    if y_label:
        hVarDic["Wmunu"].GetYaxis().SetTitle(y_label)
        
    hLabels = list(hVarDic.keys())
    hLists = [hVarDic[key] for key in hLabels]
    canv = fig.plot(hLists,
                hLabels, option="same e0",
                color=createColorTable(size=len(hLabels), color_map=61))
    fig.legend(x=[0.65,0.89], y=[0.73,0.89])
    if log: fig.log(log)
    canv.Draw()


def plot_samples_stack(fig, hVarDic, log=None, x_label=None, y_label=None, Draw_option="HIST SAME PFC PLC"):
    if x_label:
        hVarDic["Wmunu"].GetXaxis().SetTitle(x_label)
    if y_label:
        hVarDic["Wmunu"].GetYaxis().SetTitle(y_label)
        
    hLabels = list(hVarDic.keys())
    hLists = [hVarDic[key] for key in hLabels]
    canv = fig.stack(hLists, hLabels,
                color=createColorTable(size=len(hLabels), color_map=61), option=Draw_option, log_scale=log)
    
    fig.legend(x=[0.65,0.89], y=[0.73,0.89])

    canv.Draw()
    canv.RedrawAxis()
    return canv

def get_mc16(input_dir, cut):
    '''
    input_dir --- path to all data.
                  e.g input_dir/0001.root
                                0002.root
                                ....
    cut --- cut string. e.g for signal/control regions.
            example: "met>30000&&qT>30000"
            
    return {"dsid": RDF}. Dictionaries of RootDataFrame for each DSDI
    '''
    _datasetsRDF_mc16={}
    for dataset in [input_dir+f for f in os.listdir(input_dir)]:
        _df, _dsid = get_rdf(dataset,
                branches=["weight_mc", "weight_pileup", "deta_lj", "Xperp","nbjet",
                          "mT", "qT", "met", "signalMu_pt", "signalMu_charge", "mu",
                         "weight_leptonSF", "weight_bTagSF_DL1r_77","weight_jvt"])
        if _df is None: continue
        try:
            _df = _df.Filter(cut)\
                .Define("weight_total", "weight_mc*weight_pileup*weight_leptonSF*weight_bTagSF_DL1r_77*weight_jvt")\
                .Define("mT_GeV", "mT*0.001").Define("met_GeV", "met*0.001")
        except:
            print("empty file: ", _dsid)
        else:
            _datasetsRDF_mc16[_dsid]= _df
    return _datasetsRDF_mc16

def get_data16(input_dir, cut):
    '''
    input_dir --- path to all data.
                  e.g input_dir/0001.root
                                0002.root
                                ....
    cut --- cut string. e.g for signal/control regions.
            example: "met>30000&&qT>30000"
            
    return {"dsid": RDF}. Dictionaries of RootDataFrame for each DSDI
    '''
    _datasetsRDF_data16={}
    for dataset in [input_dir+f for f in os.listdir(input_dir)]:
        _df, _dsid = get_rdf(dataset,
                    branches=["deta_lj", "Xperp","nbjet", "mT", "qT",
                           "met", "signalMu_pt", "signalMu_charge", "mu"])
        if _df is None: continue
        try:
            _df = _df.Filter(cut).Define("mT_GeV", "mT*0.001").Define("met_GeV", "met*0.001")
        except:
            print("empty file: ", _dsid)
        else:
            _datasetsRDF_data16[_dsid]= _df
    return _datasetsRDF_data16

def bkg_compositions(mc_samples_hists,begin=0, end=-1):
    '''
    mc_samples_hists --- is a distionary of histograms after normalized
                         and grouped for mC processes.
                         One histogram per MC process.
    begin,end --- are start and end of bins in X axis.
                  This is used to get number of events
                  in different range in X axis
    '''
    _totalMC = 0.0
    event_counts = {}
    for key in mc_samples_hists.keys():
        event_counts[key] = mc_samples_hists[key].Integral(begin,end)
        _totalMC += event_counts[key]
    
    #print out here
    for key in event_counts.keys():
        print("%s = %.2f"%(key, 100.*event_counts[key]/_totalMC))
    return _totalMC


def save_tree(RDFs, output, **kwargs):
    '''
    Save all the rdfs to root file.
    e.g save root files after signal region
    selections.
    
    tree_name --- default is nominal
    '''
    
    tree_name = 'nominal'
    if "tree_name" in kwargs:
        tree_name = kwargs['tree_name']
        
    for dsid, _df in RDFs.items():
        _df.Snapshot(tree_name, "{0}/{1}.root".format(output,dsid))


def named_datasets(input_dir, cut="1", **kwargs):
    '''
    input_dir --- path to all data.
                  e.g input_dir/0001.root
                                0002.root
                                ....
    tree_name --- default is nominal
            
    return {"dsid": RDF}. Dictionaries of RootDataFrame for each DSDI
    '''
    tree_name = 'nominal'
    if "tree_name" in kwargs:
        tree_name = kwargs['tree_name']
        
    _datasetsRDF={}
    for dataset in [input_dir+f for f in os.listdir(input_dir)]:
        _df, _dsid = get_rdf(dataset)
        if _df is None:
            print("empty file: ", _dsid)
            continue
        try:
            _df = _df.Filter(cut) # test filter, if RDF is fine
        except:
            print("empty file: ", _dsid)
        else:
            _datasetsRDF[_dsid]= _df
    return _datasetsRDF

import ROOT
import os
import numpy as np
import logging
logging.basicConfig()
def tightLepton(dataframe, channel):
    #channel=electron or muon (default)
    ELECTRON = "el"
    MUON = "mu"
    channels = {ELECTRON: "ejets_DL1r_2016", MUON: "mujets_DL1r_2016"}
    signal_muon_str='''
    vector<float> signalMuon_%s;
    int nmu = mu_%s.size();
    for (int i=0; i<nmu; i++){
        if (mu_isTight[i]){
            signalMuon_%s.push_back(mu_%s[i]);
        }
    }
    return signalMuon_%s;
    '''
    
    signal_muon_pt = signal_muon_str.replace("%s", "pt")
    signal_muon_eta = signal_muon_str.replace("%s", "eta")
    signal_muon_phi = signal_muon_str.replace("%s", "phi")
    signal_muon_e = signal_muon_str.replace("%s", "e")
    signal_muon_charge = signal_muon_str.replace("%s", "charge")

    #define signal lepton
    if channel == MUON:
        dataframe = dataframe.Filter(channels[channel]).Define("signalMu_pt", signal_muon_pt)\
                              .Define("signalMu_eta", signal_muon_eta)\
                       .Define("signalMu_phi", signal_muon_phi).Define("signalMu_e", signal_muon_e)\
                       .Define("signalMu_charge", signal_muon_charge)
        #change channel name to signalMu
        return dataframe, "signalMu"
        
def looseLepton(dataframe, channel):
    #channel=electron or muon (default)
    ELECTRON = "el"
    MUON = "mu"
    channels = {ELECTRON: "ejets_DL1r_2016_loose", MUON: "mujets_DL1r_2016_loose"}
    if channel == MUON:
        dataframe = dataframe.Filter(channels[channel])
    return dataframe, channel
    

class wjetsAsymmetryFrame():
    """
    wjets = wjetsAsymmetryFrame(name='wjetsAsymmetryFrame', LOGLEVEL='ERROR')
    Attributes:
    wjets.mT_string: defination of mT variable.
    wjets.qTx_string: constructd W momentum along x direction.
    wjets.qTy_string: constructd W momentum along y direction.
    wjets.Xprep_string: Xprep definition.
    wjets.deta_lj_string: eta difference between lepton and leeding jet.
    wjets.preSelection_string: preselecton cut string.
    """
    def __init__(self, name='wjetsAsymmetryFrame', LOGLEVEL='ERROR'):
        self.name = name
        self.log = logging.getLogger(self.name)
        level = {"DEBUG": logging.DEBUG,
                 "INFO" : logging.INFO,
                 "WARNING" : logging.WARNING,
                 "ERROR" : logging.ERROR
                }
        self.log.setLevel( level[LOGLEVEL] )
        
        self.objectSize = '''
            int n_jet = 0;
            n_jet = {0}_pt.size();
            return n_jet
            '''
    
        self.mT_string = '''
            float mt = sqrt(2.*{0}_pt[0]*met*(1. - TMath::Cos(TVector2::Phi_mpi_pi({0}_phi[0]- met_phi))));
            return mt;
            '''

        self.qTx_string='''
            TLorentzVector lepTV = TLorentzVector();
            lepTV.SetPtEtaPhiE({0}_pt[0],{0}_eta[0],{0}_phi[0],{0}_e[0]);
            float qTx = lepTV.Px() + met_px;
            return qTx;
            '''
        self.qTy_string='''
            TLorentzVector lepTV = TLorentzVector();
            lepTV.SetPtEtaPhiE({0}_pt[0],{0}_eta[0],{0}_phi[0],{0}_e[0]);
            float qTy = lepTV.Py() + met_py;
            return qTy;
            '''

        self.Xperp_string = '''
            TLorentzVector lepTV = TLorentzVector();
            lepTV.SetPtEtaPhiE({0}_pt[0],{0}_eta[0],{0}_phi[0],{0}_e[0]);
            float Xperp = (-qTy*lepTV.Px() + qTx*lepTV.Py())/(qT*40200);
            return Xperp;
            '''

        self.deta_lj_string = "{0}_eta[0] - jet_eta[0]"
    
        self.preSelection_string="jet_pt[0]>30000&&{0}_pt[0]>30000" #met>25&&mT>50&&qT>30
        
        self.bjetSize = '''
            unsigned short int nb = 0;
            for (auto& isbtag : jet_isbtagged_DL1r_77){
                if (isbtag) nb++;
            }
            return nb;
            '''
        
        self.df=None

    def create_selected_events(self, files=None, channel='mu', loose=False, **kwargs):
        """
        Arguments:
        files: list of files (with full path).
        channel: muon or electron.
        tree_name: name of the tree in a root file default is "nominal".
        
        return:
            this function return RDataFrame objects after filters and cuts.
        """
    
        if files is None:
            self.log.error('Input file list is empty!')
            
        files_vec = ROOT.std.vector("string")()
        if not isinstance(files, list):
            files_vec.push_back(files)
        else:
            for f in files:
                files_vec.push_back(f)
        
    
        tree_name = 'nominal'
        if "tree_name" in kwargs:
            tree_name = kwargs['tree_name']
        
        
            
        data_rdf = ROOT.RDataFrame(tree_name, files_vec).Define("njet",self.objectSize.format("jet"))
        data_rdf =data_rdf.Filter('njet >0').Define("met","met_met")
        
        if loose:
            data_rdf, channel = looseLepton(data_rdf, channel)
        else:
            data_rdf, channel = tightLepton(data_rdf, channel)
        
        self.log.info("channel: %s", channel )
        self.log.info("preselection: %s", self.preSelection_string.format(channel) )
        self.log.info("mT: %s", self.mT_string.format(channel) )
        
        data_rdf = data_rdf.Filter(self.preSelection_string.format(channel))
        data_rdf = data_rdf.Define("met_px","met_met*TMath::Cos(met_phi)").Define("met_py","met_met*TMath::Sin(met_phi)")\
            .Define("mT",self.mT_string.format(channel))\
            .Define("qTx", self.qTx_string.format(channel)).Define("qTy", self.qTy_string.format(channel))\
            .Define("qT", "sqrt(qTx*qTx + qTy*qTy)").Define("Xperp", self.Xperp_string.format(channel))\
            .Define("deta_lj",self.deta_lj_string.format(channel));

        data_rdf = data_rdf.Define("nbjet", self.bjetSize)
        self.df = data_rdf
        return self.df
    
    def load_raw_data(self, files=None, **kwargs):
        """
        Load data to RDataFrame without filtering it.
        load_raw_data(files=[f1, f2, ...], **kwargs)
        Arguments:
        files: list of files (with full path).
        tree_name: name of the tree in a root file default is "nominal".
        
        return:
            this function return RDataFrame objects.
        """
    
        if files is None:
            self.log.error('Input file list is empty!')
            
        files_vec = ROOT.std.vector('string')()
        if not isinstance(files, list):
            files_vec.push_back(files)
        else:
            for f in files:
                files_vec.push_back(f)
    
        tree_name = 'nominal'
        if "tree_name" in kwargs:
            tree_name = kwargs['tree_name']
        
        _branches=None
        if "branches" in kwargs:
            branches_vec = ROOT.std.vector('string')()
            _branches = kwargs['branches']
        if _branches:
            if not isinstance(_branches, list):
                branches_vec.push_back(_branches)
            else:
                for b in _branches:
                    branches_vec.push_back(b)
            self.df = ROOT.RDataFrame(tree_name, files_vec, branches_vec)
        else:
            self.df = ROOT.RDataFrame(tree_name, files_vec)
        return self.df   
        
    def snapshot(self, treeName, fileName):        
        return self.df.Snapshot(treeName, fileName)
        




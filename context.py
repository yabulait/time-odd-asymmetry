import os, sys
sys.path.append("/eos/home-y/yabulait/wjets/")
sys.path.append("/eos/home-y/yabulait/wjets/time-odd-asymmetry")
sys.path.append("/eos/home-y/yabulait/wjets/time-odd-asymmetry/python/")

import wjetsAsymmetryFrame
import pyrootUtil
import json

from helper_functions import (get_rdf, fill_histogram, NormalizeHist, groupBy, draw_atlas_label, createColorTable, plot_samples, plot_samples_stack, get_mc16, get_data16, named_datasets)


Luminosity_fb = 33
Luminosity_pb = 32988.1
grid_date= "20200624_1530" #"20200604_1600" #"20200508_1230" #20200310_1052 20200410_1450


event_weights = None
with open("../data/{}/sample_weights.txt".format(grid_date),"r") as f:
    event_weights= json.load( f)
#possible flavours: WplvWmqq, ttbar, wjets
process_groups = {"Wenu": ["364170",  "364172",  "364174",  "364176",  "364178",  "364180",
           "364182",  "364171",  "364173",  "364175",  "364177",  "364179",
           "364181",  "364183"],
 "Wmunu" : ["364156", "364157", "364158", "364159", "364160", "364161",
           "364162", "364163", "364164", "364165", "364166", "364167",
           "364168", "364169"],
 "Wtaunu": ["364184", "364185", "364186", "364187", "364188", "364189", "364190",
           "364191", "364192", "364193", "364194", "364195", "364196", "364197"],
 "ttbar": ["410470"],
 "WWlpqq": ["363360", "363361"],
 "singleTop": ["410646", "410647", "410658", "410659"],
 "Ztautau": ["364128", "364129", "364130", "364131","364132", "364133",
            "364134", "364135", "364136", "364137", "364138", "364139",
            "364140", "364141"],
 "Zmumu": ["364100", "364101", "364102", "364103", "364104", "364105",
          "364106", "364107", "364108", "364109", "364110", "364111",
          "364112", "364113"]
}

data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/tight_selection/mu/".format(grid_date)
data_groups = {"data16" : [_f.strip(".root") for _f in os.listdir(data_dir)] }


W_boson={"+":"signalMu_charge[0]>0&&signalMu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000",
              "-":"signalMu_charge[0]<0&&signalMu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000"}
              
W_boson_loose={"+":"mu_charge[0]>0&&mu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000",
                "-":"mu_charge[0]<0&&mu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000"}
#exteded selection don't have mT cut
W_boson_extend={"+":"signalMu_charge[0]>0&&signalMu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000",
                        "-":"signalMu_charge[0]<0&&signalMu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000"}
W_boson_extend_loose={"+":"mu_charge[0]>0&&mu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000",
                        "-":"mu_charge[0]<0&&mu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000"}

#asymmetry selection have all cuts
asymmetry_region={"+":"signalMu_charge[0]>0&&signalMu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000&&abs(Xperp)>0.6",
    "-":"signalMu_charge[0]<0&&signalMu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000&&abs(Xperp)>0.6"}
asymmetry_region_loose={"+":"mu_charge[0]>0&&mu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000&&abs(Xperp)>0.6",
    "-":"mu_charge[0]<0&&mu_pt[0]>30000&&(nbjet<1)&&met>30000&&qT>30000&&mT>50000&&abs(Xperp)>0.6"}

def load_mc16( region=W_boson_extend["+"], loose=False):
    input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/tight_selection/mu/".format(grid_date)
    if loose:
        input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/loose_selection/mu/".format(grid_date)
    return get_mc16(input_dir=input_dir, cut=region)
    
    
def load_data16(region=W_boson_extend["+"], loose=False):
    _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/tight_selection/mu/".format(grid_date)
    if loose:
        _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/loose_selection/mu/".format(grid_date)
    return get_data16(input_dir=_data_dir, cut=region)
    
def load_W_boson_extend(loose=False):
    input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/W_boson_extend/mu_p/".format(grid_date)
    _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/W_boson_extend/mu_p/".format(grid_date)
    if loose:
        input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/W_boson_extend_loose/mu_p/".format(grid_date)
        _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/W_boson_extend_loose/mu_p/".format(grid_date)
    return {"mc16":named_datasets(input_dir=input_dir, cut="qT>30000"), "data16": named_datasets(input_dir=_data_dir, cut="qT>30000")}
    
def load_W_boson(loose=False):
    input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/W_boson_extend/mu_p/".format(grid_date)
    _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/W_boson_extend/mu_p/".format(grid_date)
    if loose:
        input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/W_boson_extend_loose/mu_p/".format(grid_date)
        _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/W_boson_extend_loose/mu_p/".format(grid_date)
    return {"mc16":named_datasets(input_dir=input_dir, cut="mT>50000"), "data16":named_datasets(input_dir=_data_dir, cut="mT>50000")}
    
def load_asymmetry_region(loose=False):
    input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/W_boson_extend/mu_p/".format(grid_date)
    _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/W_boson_extend/mu_p/".format(grid_date)
    if loose:
        input_dir = "/eos/home-y/yabulait/wjets/ntuples/mc16/{0}/W_boson_extend_loose/mu_p/".format(grid_date)
        _data_dir = "/eos/home-y/yabulait/wjets/ntuples/data16/{0}/W_boson_extend_loose/mu_p/".format(grid_date)
    return {"mc16":named_datasets(input_dir=input_dir, cut="mT>50000&&abs(Xperp)>0.6"), "data16":named_datasets(input_dir=_data_dir, cut="mT>50000&&abs(Xperp)>0.6")}

